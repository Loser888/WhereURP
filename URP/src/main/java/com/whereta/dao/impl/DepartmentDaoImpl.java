package com.whereta.dao.impl;

import com.whereta.dao.IDepartmentDao;
import com.whereta.mapper.DepartmentMapper;
import com.whereta.model.Department;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Vincent
 * @time 2015/9/2 10:10
 */
@Repository("departmentDao")
public class DepartmentDaoImpl implements IDepartmentDao {
    @Resource
    private DepartmentMapper departmentMapper;

    /**
     * 获取所有部门
     *
     * @return
     */
    public List<Department> selectAll() {
        return departmentMapper.selectAll();
    }

    /**
     * 获取部门
     *
     * @param list
     * @param id
     * @return
     */
    public Department get(List<Department> list, int id) {
        for (Department department : list) {
            if (department.getId() == id) {
                return department;
            }
        }
        return null;
    }

    /**
     * 创建部门
     * @param department
     * @return
     */
    public int create(Department department) {
        return departmentMapper.insertSelective(department);
    }

    /**
     * 删除部门
     * @param depId
     * @return
     */
    public int delete(int depId) {
        return departmentMapper.deleteByPrimaryKey(depId);
    }

    /**
     * 更新部门
     * @param department
     * @return
     */
    public int update(Department department) {
        return departmentMapper.updateByPrimaryKeySelective(department);
    }
}
