package com.whereta.dao.impl;

import com.whereta.dao.IPermissionDao;
import com.whereta.mapper.PermissionMapper;
import com.whereta.model.Permission;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Vincent
 * @time 2015/8/27 17:31
 */
@Repository("permissionDao")
public class PermissionDaoImpl implements IPermissionDao {
    @Resource
    private PermissionMapper permissionMapper;
    /**
     * 根据权限id获取权限
     * @param permissionList
     * @return
     */
    public Permission get(List<Permission> permissionList,int id) {
        for(Permission permission:permissionList){
            if(permission.getId().intValue()==id){
                return permission;
            }
        }
        return null;
    }



    /**
     * 获取所有权限
     * @return
     */
    public List<Permission> selectAll() {
        return permissionMapper.selectAll();
    }

    /**
     * 创建权限
     * @param permission
     * @return
     */
    @Override
    public int createPermission(Permission permission) {
        return permissionMapper.insertSelective(permission);
    }

    /**
     * 删除权限
     * @param perId
     */
    @Override
    public int deletePermission(int perId) {
        return permissionMapper.deleteByPrimaryKey(perId);
    }

    /**
     * 更新权限
     * @param permission
     * @return
     */
    @Override
    public int updatePermission(Permission permission) {
        return permissionMapper.updateByPrimaryKeySelective(permission);
    }
}
