package com.whereta.aop;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * Created by vincent on 15-9-23.
 */
@Aspect
@Component
public class ServiceLogAop {

    private static final Logger loggerRecord = Logger.getLogger("RECORD");


    @Around("execution(public * com.whereta.service.*.*(..))")
    public Object serviceAOP(ProceedingJoinPoint point) throws IOException {
        Object proceed = null;
        Signature signature = point.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        Object target = point.getTarget();
        String className = target.getClass().getName();
        try {
            Object[] args = point.getArgs();
            if (args != null) {
                for (int i = 0; i < args.length; i++) {
                    Object param = args[i];
                    if (param != null) {
                        if (param.getClass().equals(String.class)) {
                            args[i] = StringEscapeUtils.escapeHtml4(param.toString());
                        }else{
                            Field[] declaredFields = param.getClass().getDeclaredFields();
                            for(Field field:declaredFields){
                                field.setAccessible(true);
                                Object o = field.get(param);
                                if(o.getClass().equals(String.class)){
                                    field.set(param, StringEscapeUtils.escapeHtml4(o.toString()));
                                }
                            }
                        }
                    }
                }
            }
            String jsonString = JSON.toJSONString(args);

            if (loggerRecord.isInfoEnabled()) {
                loggerRecord.info("Class:" + className + "  MethodName:" + method.getName() + "  Params:" + jsonString);
            }
            proceed = point.proceed(args);
            String ReturnString = JSON.toJSONString(proceed);
            if (loggerRecord.isInfoEnabled()) {
                loggerRecord.info("ReturnMsg:<!--  Class:" + className + "  MethodName:" + method.getName() + "  Content: " + ReturnString + "  -->");
            }
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
        return proceed;
    }

}
