package com.whereta.mq.dao.impl;

import com.whereta.mq.dao.ILoginLogDao;
import com.whereta.mq.mapper.LoginLogMapper;
import com.whereta.mq.model.LoginLog;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * Created by vincent on 15-9-10.
 */
@Repository("loginLogDao")
public class LoginLogDaoImpl implements ILoginLogDao {
    @Resource
    private LoginLogMapper loginLogMapper;

    @Override
    public int save(LoginLog loginLog) {
        return loginLogMapper.insertSelective(loginLog);
    }
}
